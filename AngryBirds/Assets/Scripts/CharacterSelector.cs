﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
/*
    this class is used in the settings menu and handles the user selection by:
    1. sets the character chosed in the PlayerPrefs
    2. highlights the selected character to the player
 */
public class CharacterSelector : MonoBehaviour
{
    [SerializeField] private GameObject RedButton;
    [SerializeField] private GameObject ChuckButton;
    [SerializeField] private GameObject BombButton;
    [SerializeField] private GameObject StellaButton;

    public Text selectedBirdText;

    // Use this for initialization
    void Start()
    {
        UpdateText();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateText();
    }

    private void UpdateText()
    {
        var selected = PlayerPrefs.GetString("Character") ?? "Red";
        selectedBirdText.text = "The Selected Bird is - " + selected;
    }

    public void SetRed()
    {
        PlayerPrefs.SetString("Character", "Red");
    }

    public void SetChuk()
    {
        PlayerPrefs.SetString("Character", "Chuk");
    }

    public void SetStella()
    {
        PlayerPrefs.SetString("Character", "Stella");
    }

    public void SetBomb()
    {
        PlayerPrefs.SetString("Character", "Bomb");
    }


    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("home");
    }
}
