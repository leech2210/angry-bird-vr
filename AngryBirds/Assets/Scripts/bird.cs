﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class bird : MonoBehaviour
{
    public Rigidbody2D rb;
    public float releaseTime = .15f;
    public float endOfGameTime = 4f;
    public float maxDragDistance = 10f;
    public Rigidbody2D hook;
    public GameObject nextBird;
    public string nextLevel;

    public Sprite Red;
    public Sprite Chuck;
    public Sprite Stella;
    public Sprite Bomb;

    private bool isPressed = false;
    private bool isBirdThrown = false;

    private AudioSource audioData;
    private Vector3 startPos;
    private int birdsCounter = 0;

    private void Start()
    {
        string selectedBird = PlayerPrefs.GetString("Character");
        switch (selectedBird)
        {
            case "Red":
                GetComponent<SpriteRenderer>().sprite = Red;
                break;
            case "Chuk":
                GetComponent<SpriteRenderer>().sprite = Chuck;
                break;
            case "Stella":
                GetComponent<SpriteRenderer>().sprite = Stella;
                break;
            case "Bomb":
                GetComponent<SpriteRenderer>().sprite = Bomb;
                break;
            default:
                GetComponent<SpriteRenderer>().sprite = Red;
                break;
        }

        audioData = GetComponent<AudioSource>();
        startPos = rb.position;
    }

    void Update()
    {
        // get joystick direction
        Vector3 inputDirection = Vector3.zero;
        inputDirection.x = Input.GetAxis("Mouse X") * 10f;
        inputDirection.y = -Input.GetAxis("Mouse Y") * 10f;

        rb.position = startPos + inputDirection;

        Vector2 joystickPos = new Vector2(rb.position.x, rb.position.y);
        if (Vector3.Distance(joystickPos, hook.position) > maxDragDistance)
            rb.position = hook.position + (joystickPos - hook.position).normalized * maxDragDistance;
        else
            rb.position = joystickPos;


        // check if birs is released
        if (Input.GetKey(KeyCode.Joystick1Button4))
        {
            isPressed = true;
            rb.isKinematic = false;
        }

        // if bird released - calc the vector to throw
        if (isPressed)
        {
            audioData.Play(0);
            StartCoroutine(Release());
        }

        // if bird is thrown - destroy the bird after 4 seconds
        if (isBirdThrown)
            StartCoroutine(DestroyAfter(4));
    }

    IEnumerator DestroyAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Destroy(gameObject);
    }

    IEnumerator Release()
    {
        isBirdThrown = true;

        yield return new WaitForSeconds(releaseTime);
        GetComponent<SpringJoint2D>().enabled = false;

        this.enabled = false;

        yield return new WaitForSeconds(2f);

        if (nextBird != null)
            nextBird.SetActive(true);
        else
        {
            // if no more enemy and there is another level - move to next level
            if (enemy.enemyAlive <= 0 && !string.IsNullOrEmpty(nextLevel))
            {
                enemy.enemyAlive = 0;
                SceneManager.LoadScene(nextLevel);
            }  
            else // there are more enemy to destroy or there isn't any more levels
            {
                enemy.enemyAlive = 0;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }
}