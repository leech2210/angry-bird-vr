﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GUI_Manager : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Play()
    {
        SceneManager.LoadScene("level1");
    }

    public void Settings()
    {
        SceneManager.LoadScene("settings");
    }

    public void Home()
    {
        SceneManager.LoadScene("Home");
    }
}
