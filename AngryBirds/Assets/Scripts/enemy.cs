﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class enemy : MonoBehaviour {

    public float health = 3f;
    public GameObject deathEffect;
    public static int enemyAlive = 0;
    public string nextLevel;

    public Text scoreText;
    public int pigValue;

    private static int totalScore = 0;

    private void Start()
    {
        Debug.Log(enemyAlive);
        totalScore = 0;
        enemyAlive++;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.magnitude > health)
        {
            Die();
            totalScore += pigValue;
            scoreText.text = "Score: " + totalScore.ToString();
        }
    }

    private void Die() {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        enemyAlive--;
        if (enemyAlive <= 0)
        {
            if (!string.IsNullOrEmpty(nextLevel))
            {
                enemyAlive = 0;
                SceneManager.LoadScene(nextLevel);
            }
            else
            {
                enemyAlive = 0;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }

        Destroy(gameObject);
    }
}
